README
======

This README discusses issues unique to NuttX configurations for the
STM32L476RG based PMDC motor controller board by Sebastien Lorquet

  Microprocessor: 32-bit ARM Cortex M4 at 80MHz STM32F476RGT6

Board features:

  Peripherals:
* 16 MHz XTAL for HSE
* SPI flash (64 Mb) for firmware update and data logging (SPI2)
    PB13 SCLK
    PB14 MISO
    PB15 MOSI
    PA4  CS
* I2C EEPROM for NV parameters (SO8N, I2C2)
    PB11 SDA through PA5
    PB10 SCL through PA6
* CAN transceiver for CANopen control (CAN1)
    PB8  CANRX
    PB9  CANTX
* RS-485 transceiver not used at the moment (UART4)
    PA0  TX
    PA1  RX
    PA2  TXEN
* Debug Console UART (USART1)
    PB6  TXOUT
    PB7  RXIN
* Interface to a full bridge power stage using complementary PWM outputs (TIM1)
    PA8  PWM_H_1
    PA7  PWM_L_1
    PA9  PWM_H_2
    PB0  PWM_L_2
* Interface to a quadrature encoder with optional index (TMR3)
    PC7  ENC_A
    PC6  ENC_B
    PA10 ENC_Z (EXTIRQ)
* Interface to two endstops (main/aux) (GPIO/EXTIRQ)
    PA11 Main
    PA12 Aux
* Interface to read current from an analog channel. Current values depend on power board.
    PA3  ADC1_IN8 (scaled from 0..5V to 0..3V3 via a resistive divider, INA286 output Z is 1.5 ohms)
* Cycle-by-cycle current limitation using built-in comparator (Comparator 1). Negative (max current) input driven by a DAC.
    PC5  COMP1INP (also ADC1_IN14)
* Status LEDs
    PC13 RS-485 traffic
    PC14 CAN traffic
    PC10 Status Red
    PC11 Status Orange
    PC12 Status Green
* DAC for TIM1 PWM BRK comparator trigger: DACx
* Timer for app clocking : TIMx
* Timer for ADC sampling : TODO later

  Debug:          Serial wire debug ONLY

  No debugger is included for space saving, but a STLINK from a nucleo or any
  other SWD debugger can be used.

Contents
========

  - Installation
  - Development Environment
  - GNU Toolchain Options
  - IDEs
  - NuttX EABI "buildroot" Toolchain
  - NXFLAT Toolchain
  - Hardware
  - Configurations

Installation
============
This is a custom board configuration. It is not to be included in upstream nuttx repos
since this board is not for sale.

To install this configuration, just check it out in the configs repository, or add it as a submodule:

$ cd nuttx/configs
$ git clone https://bitbucket.org/slorquet/grxmotctl.git

Then configure nuttx as if the board was a canned nuttx config:

$ (cd tools; ./configure.sh grxmotctl/nsh)
$ make

CXX initialization has been included here instead of apps/ to avoid relying on apps/platform.

Development Environment
=======================

  Either Linux or Cygwin on Windows can be used for the development environment.
  The source has been built only using the GNU toolchain (see below).  Other
  toolchains will likely cause problems.

  Build has only be tested on Linux using the launchpad ARM-GCC toolchain.
  Other toolchains may be usable, refer to the documentation of other boards.

IDEs
====

  NuttX is built using command-line make.  It can be used with an IDE, but some
  effort will be required to create the project.

  Makefile Build
  --------------
  Under Eclipse, it is pretty easy to set up an "empty makefile project" and
  simply use the NuttX makefile to build the system.  That is almost for free
  under Linux.  Under Windows, you will need to set up the "Cygwin GCC" empty
  makefile project in order to work with Windows (Google for "Eclipse Cygwin" -
  there is a lot of help on the internet).

  Using Sourcery CodeBench from http://www.mentor.com/embedded-software/sourcery-tools/sourcery-codebench/overview
    Download and install the latest version (as of this writting it was
    sourceryg++-2013.05-64-arm-none-eabi)

   Import the  project from git.
     File->import->Git-URI, then import a Exiting code as a Makefile progject
     from the working directory the git clone was done to.

   Select the Sourcery CodeBench for ARM EABI. N.B. You must do one command line
     build, before the make will work in CodeBench.

  Native Build
  ------------
  Here are a few tips before you start that effort:

  1) Select the toolchain that you will be using in your .config file
  2) Start the NuttX build at least one time from the Cygwin command line
     before trying to create your project.  This is necessary to create
     certain auto-generated files and directories that will be needed.
  3) Set up include pathes:  You will need include/, arch/arm/src/stm32,
     arch/arm/src/common, arch/arm/src/armv7-m, and sched/.
  4) All assembly files need to have the definition option -D __ASSEMBLY__
     on the command line.

  Startup files will probably cause you some headaches.  The NuttX startup file
  is arch/arm/src/stm32/stm32_vectors.S.  With RIDE, I have to build NuttX
  one time from the Cygwin command line in order to obtain the pre-built
  startup object needed by RIDE.

Hardware
========

  See include/board.h

Configurations
==============

  nsh:
  ---------
    Configures the NuttShell (nsh) located at apps/examples/nsh for the
    board.  The Configuration enables the serial interfaces on UART2.
    Support for builtin applications is enabled, but in the base
    configuration no builtin applications are selected (see NOTES below).

    NOTES:

    1. This configuration uses the mconf-based configuration tool.  To
       change this configuration using that tool, you should:

       a. Build and install the kconfig-mconf tool.  See nuttx/README.txt
          see additional README.txt files in the NuttX tools repository.

       b. Execute 'make menuconfig' in nuttx/ in order to start the
          reconfiguration process.

    2. By default, this configuration uses the CodeSourcery toolchain
       for Linux.  That can easily be reconfigured, of course.

       CONFIG_HOST_LINUX=y                     : Builds under Linux
       CONFIG_ARMV7M_TOOLCHAIN_CODESOURCERYL=y : CodeSourcery for Linux

