/****************************************************************************
 * src/stm32l4_i2c.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>
#include <syslog.h>
#include <errno.h>

#include <nuttx/eeprom/i2c_xx24xx.h>
#include <nuttx/i2c/i2c_master.h>
#include <arch/board/board.h>
#include <up_arch.h>
#include <chip.h>
#include <stm32l4.h>

#include "grxmotctl.h"

#if !defined(CONFIG_STM32L4_I2C2)
#error this board requires I2C2
#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Enables debug output from this file */

#ifndef CONFIG_DEBUG
#  undef CONFIG_DEBUG_I2C
#  undef CONFIG_DEBUG_VERBOSE
#endif

#ifdef CONFIG_DEBUG_I2C
#  define i2cerror  llerr
#  ifdef CONFIG_DEBUG_VERBOSE
#    define i2cinfo llinfo
#  else
#    define i2cinfo(x...)
#  endif
#else
#  define i2cerror(x...)
#  define i2cinfo(x...)
#endif

/************************************************************************************
 * Public Data
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32l4_i2cinitialize
 *
 * Description:
 *   Called to configure I2C chip select GPIO pins.
 *
 ************************************************************************************/

void stm32l4_i2cinitialize(void)
{
  int ret;
  FAR struct i2c_master_s * i2c2;
#if 0
  i2c2 = stm32l4_i2cbus_initialize(2);

  /* Setup the I2C EEPROM */
  ret = ee24xx_initialize(i2c2, 0xA0, "/dev/eeprom", EEPROM_M24M01, false);
  if(ret)
    {
      syslog(LOG_ERR, "Failed to initialize the I2C EEPROM\n");
    }
  else
    {
      syslog(LOG_NOTICE, "/dev/eeprom initialized\n");
    }
#endif
}

