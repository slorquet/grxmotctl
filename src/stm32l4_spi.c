/****************************************************************************
 * src/stm32l4_spi.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <stdbool.h>
#include <debug.h>
#include <syslog.h>
#include <errno.h>

#include <nuttx/spi/spi.h>
#include <arch/board/board.h>
#include <nuttx/eeprom/spi_xx25xx.h>
#include <nuttx/mtd/mtd.h>
#include <nuttx/fs/fs.h>

#include <up_arch.h>
#include <chip.h>
#include <stm32l4.h>

#include "grxmotctl.h"

#if !defined(CONFIG_STM32L4_SPI2)
#error SPI2 required by this board
#endif

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Enables debug output from this file */

#ifndef CONFIG_DEBUG
#  undef CONFIG_DEBUG_SPI
#  undef CONFIG_DEBUG_VERBOSE
#endif

#ifdef CONFIG_DEBUG_SPI
#  define spierror  llerr
#  ifdef CONFIG_DEBUG_VERBOSE
#    define spiinfo llinfo
#  else
#    define spiinfo(x...)
#  endif
#else
#  define spierror(x...)
#  define spiinfo(x...)
#endif

/************************************************************************************
 * Public Data
 ************************************************************************************/

FAR struct mtd_dev_s *mtd0;

/************************************************************************************
 * Public Functions
 ************************************************************************************/

/************************************************************************************
 * Name: stm32l4_spiinitialize
 *
 * Description:
 *   Called to configure SPI chip select GPIO pins.
 *
 ************************************************************************************/

void weak_function stm32l4_spiinitialize(void)
{
  int ret;
#ifdef CONFIG_STM32L4_SPI2
  FAR struct spi_dev_s * spi2;
#endif

  FAR struct mtd_dev_s *mtdfw;
  FAR struct mtd_dev_s *mtdstorage;


  /* Setup CS, EN & IRQ line IOs */
  stm32l4_configgpio(GPIO_CS_FLASH);

  spi2 = stm32l4_spibus_initialize(2);

  /* Setup the SPI flash */
  mtd0 = sst26_initialize_spi(spi2);
  if(mtd0 == NULL)
    {
      syslog(LOG_ERR, "Failed to initialize the SPI Flash\n");
      return;
    }
  else
    {
      syslog(LOG_NOTICE, "mtd0 initialized\n");
    }

  /*
   * Normal config: flash partitions
   * mtd0p1 is used for firmware update: 1 MB,  4096 pages/blocks,  256 sectors
   * mtd0p2 is used for data storage   : 7 MB, 28672 pages/blocks, 1972 sectors
   * -------------------------------------------------------------------
   * Total:                              8 MB, 32768 pages/blocks, 2048 sectors
   */
  mtdfw      = mtd_partition(mtd0, 0   , 4096 );
  mtdstorage = mtd_partition(mtd0, 4096, 28672);
#ifdef CONFIG_MTD_PARTITION_NAMES
  mtd_setpartitionname(mtdfw     , "fw"     );
  mtd_setpartitionname(mtdstorage, "storage");
#endif

  /* Setup the flash translation layer for mtd0p0 (fw update) */
  ret = ftl_initialize(0, mtdfw);
  if (ret < 0)
    {
      syslog(LOG_ERR, "Failed to initialize the FTL layer for mtd0p0: %d, errno %d\n", ret, errno);
      return;
    }
  else
    {
      syslog(LOG_NOTICE, "/dev/mtdblock0 initialized for fw update\n");
    }

#ifdef CONFIG_MTD_SMART
  /* Setup a smartfs volume on mtd0p2 (storage) */
  syslog(LOG_NOTICE, "Starting SMART scan\n");
  ret = smart_initialize(0, mtdstorage, NULL);
  if (ret < 0)
    {
      syslog(LOG_ERR, "Failed to initialize the SMART layer: %d, errno %d\n", ret, errno);
      return;
    }
  else
    {
      syslog(LOG_NOTICE, "/dev/smart0 initialized for storage\n");
    }
#else
  /* Setup the flash translation layer for mtd0p1 (storage) */
  ret = ftl_initialize(1, mtdstorage);
  if (ret < 0)
    {
      syslog(LOG_ERR, "Failed to initialize the FTL layer for mtd0p1: %d, errno %d\n", ret, errno);
      return;
    }
  else
    {
      syslog(LOG_NOTICE, "/dev/mtdblock1 initialized for storage\n");
    }
#endif

}

/****************************************************************************
 * Name:  stm32l4_spi1/2/3select and stm32l4_spi1/2/3status
 *
 * Description:
 *   The external functions, stm32l4_spi1/2/3select and stm32l4_spi1/2/3status must be
 *   provided by board-specific logic.  They are implementations of the select
 *   and status methods of the SPI interface defined by struct spi_ops_s (see
 *   include/nuttx/spi/spi.h). All other methods (including up_spiinitialize())
 *   are provided by common STM32 logic.  To use this common SPI logic on your
 *   board:
 *
 *   1. Provide logic in stm32l4_board_initialize() to configure SPI chip select
 *      pins.
 *   2. Provide stm32l4_spi1/2/3select() and stm32l4_spi1/2/3status() functions in your
 *      board-specific logic.  These functions will perform chip selection and
 *      status operations using GPIOs in the way your board is configured.
 *   3. Add a calls to up_spiinitialize() in your low level application
 *      initialization logic
 *   4. The handle returned by up_spiinitialize() may then be used to bind the
 *      SPI driver to higher level logic (e.g., calling
 *      mmcsd_spislotinitialize(), for example, will bind the SPI driver to
 *      the SPI MMC/SD driver).
 *
 ****************************************************************************/

void stm32l4_spi2select(FAR struct spi_dev_s *dev, uint32_t devid, bool selected)
{
  spiinfo("devid: %d CS: %s\n", (int)devid, selected ? "assert" : "de-assert");

  if(devid==SPIDEV_FLASH)
    {
      stm32l4_gpiowrite(GPIO_CS_FLASH, !selected);
    }
}

uint8_t stm32l4_spi2status(FAR struct spi_dev_s *dev, uint32_t devid)
{
  return 0;
}

/****************************************************************************
 * Name: stm32l4_spi1cmddata
 *
 * Description:
 *   Set or clear the SH1101A A0 or SD1306 D/C n bit to select data (true)
 *   or command (false). This function must be provided by platform-specific
 *   logic. This is an implementation of the cmddata method of the SPI
 *   interface defined by struct spi_ops_s (see include/nuttx/spi/spi.h).
 *
 * Input Parameters:
 *
 *   spi - SPI device that controls the bus the device that requires the CMD/
 *         DATA selection.
 *   devid - If there are multiple devices on the bus, this selects which one
 *         to select cmd or data.  NOTE:  This design restricts, for example,
 *         one one SPI display per SPI bus.
 *   cmd - true: select command; false: select data
 *
 * Returned Value:
 *   None
 *
 ****************************************************************************/

#ifdef CONFIG_SPI_CMDDATA

int stm32l4_spi2cmddata(FAR struct spi_dev_s *dev, uint32_t devid, bool cmd)
{
  return OK;
}

#endif /* CONFIG_SPI_CMDDATA */

  /* ======================================================================= */
#if 0
  /* only for tests */
  /* Setup the flash translation layer for mtd0 */
  ret = ftl_initialize(0, mtd0);
  if (ret < 0)
    {
      spierror("ERROR: Failed to initialize the FTL layer: %d, errno %d\n", ret, errno);
      return;
    }
  else
    {
      spiinfo("/dev/mtdblock0 initialized\n");
    }

  /* Setup bch for mtdblock0 */
  ret = bchdev_register("/dev/mtdblock0", "/dev/mtdchar0", false);
  if (ret < 0)
    {
      spierror("ERROR: Failed to initialize the bch layer: %d, errno %d\n", ret, errno);
    }
  else
    {
      spiinfo("/dev/mtdchar0 initialized\n");
    }
#endif

