#!/bin/sh
openocd $@ \
	-f interface/ftdi/jtag-lock-pick_tiny_2.cfg \
	-c "transport select swd" \
	-f target/stm32l4x.cfg \
	-c "reset_config srst_only" \
	-c "init" \
	-c "reset" \
	-c "exit"
