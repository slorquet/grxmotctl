/************************************************************************************
 * include/board.h
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __INCLUDE_BOARD_H
#define __INCLUDE_BOARD_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>
#ifndef __ASSEMBLY__
# include <stdint.h>
#endif

#include <stm32l4.h>

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Clocking *************************************************************************/

#if !defined(CONFIG_ARCH_CHIP_STM32L476RG)
#error this board is for STM32L476RG
#endif

#include <arch/board/grxmotctl.h>

/* DMA Values defined in arch/arm/src/stm32l4/chip/stm32l4x6xx_dma.h */

/*--------------------*/
/* UART CONFIGURATION */
/*--------------------*/

/* UART1 - no clock (P11) RX(PB7) TX(PB6) */

#define GPIO_USART1_RX    GPIO_USART1_RX_2
#define GPIO_USART1_TX    GPIO_USART1_TX_2
#define DMACHAN_USART1_RX DMACHAN_USART1_RX_1

/* UART2 Not available */
/* UART3 Not available */

/* UART4 (P9, RS485) RX(PA1) TX(PA0) EN(PA2) */
/* Transceiver : */

#define GPIO_UART4_RX   GPIO_UART4_RX_1
#define GPIO_UART4_TX   GPIO_UART4_TX_1
#define GPIO_UART4_RS485_DIR (GPIO_PORTA|GPIO_PIN2|GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)
/* No DMA line choice */

/* UART5 Not available */

/*-------------------*/
/* SPI CONFIGURATION */
/*-------------------*/

/*SPI1 Not Available */

/*SPI2 (Flash) MISO(PB14) MOSI(PB15) SCLK(PB13) */
#define GPIO_SPI2_MISO   GPIO_SPI2_MISO_1
#define GPIO_SPI2_MOSI   GPIO_SPI2_MOSI_1
#define GPIO_SPI2_SCK    GPIO_SPI2_SCK_2
/* No DMA line choice */

/*SPI3 Not Available */

/*-------------------*/
/* I2C CONFIGURATION */
/*-------------------*/

/* I2C1 Not available */

/* I2C2 EEPROM SDA(PB11) SCL(PB10) */

#define GPIO_I2C2_SDA    GPIO_I2C2_SDA_1
#define GPIO_I2C2_SCL    GPIO_I2C2_SCL_1
#define DMACHAN_I2C2_RX  DMACHAN_I2C2_RX_1
#define DMACHAN_I2C2_TX  DMACHAN_I2C2_TX_1

/* I2C3 Not available */

/*-------------------*/
/* CAN CONFIGURATION */
/*-------------------*/

/* CAN1 (P16) TX(PB9) RX(PB8) */
/* Using SN65HVD233 transceiver on power board */
/* Pins shared with OTGFS */
#define GPIO_CAN1_TX     GPIO_CAN1_TX_2
#define GPIO_CAN1_RX     GPIO_CAN1_RX_2

/*----------------------*/
/* SPI CS CONFIGURATION */
/*----------------------*/

#define GPIO_CS_FLASH  (GPIO_PORTC|GPIO_PIN6 |GPIO_OUTPUT|GPIO_SPEED_50MHz|GPIO_PUSHPULL|GPIO_OUTPUT_SET)

/*-------------------*/
/* TIM CONFIGURATION */
/*-------------------*/

/* Motor PWM outputs */
#define GPIO_TIM1_CH1OUT  GPIO_TIM1_CH1OUT_1
#define GPIO_TIM1_CH1NOUT GPIO_TIM1_CH1N_1
#define GPIO_TIM1_CH2OUT  GPIO_TIM1_CH2OUT_1
#define GPIO_TIM1_CH2NOUT GPIO_TIM1_CH2N_1

/* Quadrature encoder */
#define GPIO_TIM3_CH1IN GPIO_TIM3_CH1IN_3
#define GPIO_TIM3_CH2IN GPIO_TIM3_CH2IN_3

/*-------------------*/
/* LED CONFIGURATION */
/*-------------------*/

/* To save energy when the LED is saved, LEDs are wired open drain:
 *   - When the I/O is LOW, the LED is on.
 *   - When the I/O is HIGH or FLOATING, the LED is off. Floating is preferred to save current.
 */

#define GPIO_LED_RS485 (GPIO_PORTC|GPIO_PIN13|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)
#define GPIO_LED_CAN   (GPIO_PORTC|GPIO_PIN14|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)
#define GPIO_LED_RED   (GPIO_PORTC|GPIO_PIN10|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)
#define GPIO_LED_ORNG  (GPIO_PORTC|GPIO_PIN11|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)
#define GPIO_LED_GRN   (GPIO_PORTC|GPIO_PIN12|GPIO_OUTPUT|GPIO_OPENDRAIN|GPIO_OUTPUT_SET)

/* LED index values for use with board_userled() */

enum
{
  BOARD_LED_RS485,
  BOARD_LED_CAN,
  BOARD_LED_RED,
  BOARD_LED_ORNG,
  BOARD_LED_GRN,
  BOARD_NLEDS
};

/* LED bits for use with board_userled_all() */

#define BOARD_LED_RS485_BIT  (1 << BOARD_LED_RS485)
#define BOARD_LED_CAN_BIT    (1 << BOARD_LED_CAN)
#define BOARD_LED_RED_BIT    (1 << BOARD_LED_RED)
#define BOARD_LED_ORNG_BIT   (1 << BOARD_LED_ORNG)
#define BOARD_LED_GRN_BIT    (1 << BOARD_LED_GRN)

/************************************************************************************
 * Public Data
 ************************************************************************************/

#ifndef __ASSEMBLY__

#undef EXTERN
#if defined(__cplusplus)
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/************************************************************************************
 * Public Function Prototypes
 ************************************************************************************/
/************************************************************************************
 * Name: stm32l4_board_initialize
 *
 * Description:
 *   All STM32L4 architectures must provide the following entry point.  This entry point
 *   is called early in the initialization -- after all memory has been configured
 *   and mapped but before any devices have been initialized.
 *
 ************************************************************************************/

void stm32l4_board_initialize(void);

#undef EXTERN
#if defined(__cplusplus)
}
#endif

#endif /* __ASSEMBLY__ */
#endif  /* __INCLUDE_BOARD_H */
